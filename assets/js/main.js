$(function(){
  if ($(window).width() > 991) {
    $(".workarea-block_cnts").css("height",($(window).height()-96) +"px");
    $(".workarea-block_filter").css("height",($(window).height()-96) +"px");
  }

  /*Main navigation
   ---------------------------------------------------------------------*/
  $(document).on("click",".nav-parent a",function(ev){
    if($("#wrapper").hasClass("sidebar-collapsed")) {
      ev.preventDefault();
    }
    else {
      $(this).parent().addClass("collapsed");
      $(this).next(".nav-children_ul").slideToggle();
    }
  });
  $(document).on("click",".collapsed a",function(){
    $(this).parent().removeClass("collapsed");
  });

  /*Main mobile-navigation
   ---------------------------------------------------------------------*/
  $(document).on("click",".navigation-mobile-parent a",function(ev){
    $(this).parent().addClass("collapsed");
    $(this).next(".navigation-mobile-children_ul").slideToggle();
  });
  $(document).on("click",".collapsed a",function(){
    $(this).parent().removeClass("collapsed");
  });

  /* Toggle mobile-navigation */
  $(document).on("click",".top-bar-mobile_toggler a",function(){
    $(this).parent().addClass("menu-collapsed");
    $(this).parent().parent().next().slideToggle();
  });

  $(document).on("click",".menu-collapsed a",function(){
    $(this).parent().removeClass("menu-collapsed");
  });

  /* Toggle sidebar
   ---------------------------------------------------------------------*/
  $(document).on("click",".top-bar_toggler a",function(){
    $(".sidebar-left").mCustomScrollbar("destroy");
    $(this).parent().addClass("toggled");
    $("#wrapper").addClass("sidebar-collapsed");
    /*hide children ul if it was opened*/
    $(this).closest(".main-content").prev().find(".nav-children_ul").hide();
    $(this).closest(".top-bar").animate({
      left: "60px"
    },5);
    $(this).closest(".main-content").animate({
      marginLeft: "60px"
    },5);
    $(this).closest(".main-content").prev().animate({
        width: "60px"
      },5);
  });

  if ($(window).width() > 1199 && $(window).width() < 1400) {
    $(document).on("click",".toggled a",function(){
      $(".sidebar-left").mCustomScrollbar({
        theme:"minimal-dark"
      });
      $(this).parent().removeClass("toggled");
      $(this).closest(".top-bar").animate({
        left: "240px"
      },5);
      $(this).closest(".main-content").animate({
        marginLeft: "240px"
      },5);
      $(this).closest(".main-content").prev().animate({
        width: "240px"
      },5,function(){
        $("#wrapper").removeClass("sidebar-collapsed");
      });
    });
  }

  if ($(window).width() > 992 && $(window).width() < 1199) {
//    $(".top-bar_toggler a").trigger("click");
    $(document).on("click",".toggled a",function(){
      $(".sidebar-left").mCustomScrollbar({
        theme:"minimal-dark"
      });
      $(this).parent().removeClass("toggled");
      $(this).closest(".top-bar").animate({
        left: "200px"
      },5);
      $(this).closest(".main-content").animate({
        marginLeft: "200px"
      },5);
      $(this).closest(".main-content").prev().animate({
        width: "200px"
      },5,function(){
        $("#wrapper").removeClass("sidebar-collapsed");
      });
    });
  }
  else {
    $(document).on("click", ".toggled a", function () {
      $(".sidebar-left").mCustomScrollbar({
        theme: "minimal-dark"
      });
      $(this).parent().removeClass("toggled");
      $(this).closest(".top-bar").animate({
        left: "292px"
      }, 5);
      $(this).closest(".main-content").animate({
        marginLeft: "292px"
      }, 5);
      $(this).closest(".main-content").prev().animate({
        width: "292px"
      }, 5, function () {
        $("#wrapper").removeClass("sidebar-collapsed");
      });
    });
  }

  /* News-ntfs
   ---------------------------------------------------------------------*/
  if ($(window).width() > 992 && $(window).width() < 1199) {
    $(document).on("click", "#news-ntf", function () {
      $(this).addClass("news-notif active");
      $(".news-ntf").show().animate({
        right: 0
      }, 500, "linear");
      //TODO AJAX REQUEST
    });


    $(document).on("click", ".news-notif", function () {
      $(".news-ntf").animate({
        right: "-255px"
      }, 250, "linear", function () {
        $(".news-ntf").hide();
        $(".news-notif").removeClass("news-notif active");
      });
    });
  }
  else {
    $(document).on("click", "#news-ntf", function () {
      $(this).addClass("news-notif active");
      $(".news-ntf").show().animate({
        right: 0
      }, 500, "linear");
      //TODO AJAX REQUEST
    });


    $(document).on("click", ".news-notif", function () {
      $(".news-ntf").animate({
        right: "-308px"
      }, 250, "linear", function () {
        $(".news-ntf").hide();
        $(".news-notif").removeClass("news-notif active");
      });
    });
  }
  /* Tasks-ntfs
   ---------------------------------------------------------------------*/
  $(document).on("click","#tasks-ntf",function(){
    $(this).addClass("tasks-notif active");
    $(this).next(".tasks-ntf").fadeIn("normal");
  });
  $(document).on("click",".tasks-notif",function(){
    $(this).removeClass("tasks-notif active");
    $(this).next(".tasks-ntf").fadeOut("normal");
  });

  /* Mobile filter toggling */
  $(document).on("click",".workarea-block-mobile_filter_t",function(){
    $(this).addClass("mobile-filter-collapsed");
    $(this).next().slideToggle("slow");
  });

  $(document).on("click",".mobile-filter-collapsed",function(){
    $(this).removeClass("mobile-filter-collapsed");
  });

  /* Scrollbar
   ---------------------------------------------------------------------*/
  if ($(window).width() < 992) {
    $("body").mCustomScrollbar({
      theme:"minimal-dark"
    });
  }
  $(".sidebar-left").mCustomScrollbar({
    theme:"minimal-dark"
  });
  $(".news-ntf").mCustomScrollbar({
    theme:"minimal-dark"
  });
  $(".tasks-ntf").mCustomScrollbar({
    theme:"minimal-dark"
  });

  /* Dropdownlist
   ---------------------------------------------------------------------*/
  $(".dropdown dt a").on('click', function () {
    $(this).closest(".dropdown").find("ul").slideToggle('fast');
  });

  $(".dropdown dd ul li a").on('click', function () {
    $(this).parent().parent().hide();
  });

  function getSelectedValue(id) {
    return $("#" + id).find("dt a span.value").html();
  }

  $(document).bind('click', function (e) {
    var $clicked = $(e.target);
    if (!$clicked.parents().hasClass("dropdown")) $(".dropdown dd ul").hide();
  });
  $('.mutliSelect input[type="checkbox"]').on('click', function () {
    var title = $(this).closest('.mutliSelect').find('input[type="checkbox"]').val(),
        title = $(this).val() + ",";
    if ($(this).is(':checked')) {
      var html = '<span title="' + title + '">' + title + '</span>';
      $(this).closest(".dropdown").find(".multiSel").append(html);
      $(this).closest(".dropdown").find(".hida").hide();
    }
    else {
      $('span[title="' + title + '"]').remove();
      var ret = $(".hida");
      $(this).closest(".mutliSelect").find(".dropdown dt a").append(ret);
    }
  });
  <!-- /Dropdownlist -->

  /* Table settings draggable select options
   ---------------------------------------------------------------------*/

  $(document).on("click","#add_col", function(){
    var selected_el = $("#firstSelect").val();
    if (selected_el.length > 0) {
      selected_el.forEach(function(el){
        $("#firstSelect option[value="+el+"]").clone().appendTo("#secondSelect");
        $("#firstSelect option[value="+el+"]").remove();
      });
    }
  });

  $(document).on("click","#rem_col", function(){
    var selected_el2 = $("#secondSelect").val();
    if (selected_el2.length > 0) {
      selected_el2.forEach(function(el){
        $("#secondSelect option[value="+el+"]").clone().appendTo("#firstSelect");
        $("#secondSelect option[value="+el+"]").remove();
      });
    }
  });

  $(document).on("click","#up", function() {
    var $selected_first;
    $($("#secondSelect option").get().reverse()).each(function () {
      var $selected = $(this).prop('selected');

      if (!$selected_first) {
        if ($selected) {
          $selected_first = $(this);
        }
        else {
          return;
        }
      }
      else {
        if ($selected) {
          return;
        }
        else {
          var $not_selected = $(this).detach();
          $not_selected.insertAfter($selected_first);
          $selected_first = null;
        }
      }
    });
  });

  $(document).on("click","#down", function() {
    var $selected_first;
    $("#secondSelect option").each(function () {
      var $selected = $(this).prop('selected');

      if (!$selected_first) {
        if ($selected) {
          $selected_first = $(this);
        }
        else {
          return;
        }
      }
      else {
        if ($selected) {
          return;
        }
        else {
          var $not_selected = $(this).detach();
          $not_selected.insertBefore($selected_first);
          $selected_first = null;
        }
      }
    });
  });

  /* Toggle hidden additional fields for
  ---------------------------------------------------------------------- */
  $(document).on("click", ".show-add-fields", function () {
    $(this).closest(".form-horizontal").find(".additional-fields").slideToggle();
    $(this).addClass("open");
    $(this).find(".plus").removeClass("plus").addClass("minus");
  });

  $(document).on("click", ".open", function () {
    $(this).removeClass("open");
    $(this).find(".minus").removeClass("minus").addClass("plus");
  });

  /* Upload img in a browser storage
  ------------------------------------------------------------------------*/
  $("#User_image").change(function(){
    var fileinput = this;
    if (fileinput.files && fileinput.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        console.log(e.target.result);
        $(fileinput).parent().parent().find('img').attr('src', e.target.result);
      }

      reader.readAsDataURL(fileinput.files[0]);
    }
  });

  /* New deal
  ------------------------------------------------------------------------  */
  $(document).on("click","#new-deal",function(){
    $(this).next().slideDown();
  });
  $(document).on("click","#deal-cancel",function(){
    $(this).closest(".deal-add_block").slideUp();
  });

  /* functionList
   ---------------------------------------------------------------------*/
  checkboxesInTable();


});

$(window).resize(function(){
  if ($(window).width() > 991) {
    $(".workarea-block_cnts").css("height",($(window).height()-96) +"px");
    $(".workarea-block_filter").css("height",($(window).height()-96) +"px");
  }
});

function checkboxesInTable() {
  /* checkAll click
   ---------------------------------------------------------------------*/
  $(document).on("click", "#checkall", function () {
    $(".table-crm input[type=checkbox]").removeClass("all-checked").removeClass("chkb-checked").prop("checked", false);
    $(".table-crm .__ttip").hide();
    $(this).addClass("all-checked");
    $(this).closest(".table-crm").find("input[type=checkbox]").prop("checked", true);
    $(this).parent().find(".__ttip").show();
  });
  $(document).on("click", ".all-checked", function () {
    $(".table-crm input[type=checkbox]").removeClass("all-checked").removeClass("chkb-checked").prop("checked", false);
    $(".table-crm .__ttip").hide();
  });
  <!-- /checkAll click -->

  /* checkbox click
   ---------------------------------------------------------------------*/
  $(document).on("click", ".check-this", function () {
    if ($("#checkall").hasClass("all-checked")) {
      $(".table-crm input[type=checkbox]").removeClass("all-checked").removeClass("chkb-checked").prop("checked", false);
      $(".table-crm .__ttip").hide();
      $(this).addClass("chkb-checked");
      $(this).prop("checked", true);
      $(this).parent().find(".__ttip").show();
    }
    else {
      $(".table-crm .__ttip").hide();
      $(this).addClass("chkb-checked");
      $(this).prop("checked", true);
      $(this).parent().find(".__ttip").show();
    }
  });
  $(document).on("click", ".chkb-checked", function () {
    if ($("#checkall").hasClass("all-checked")) {
      $(".table-crm input[type=checkbox]").removeClass("all-checked").removeClass("chkb-checked").prop("checked", false);
      $(".table-crm .__ttip").hide();
    }
    $(this).removeClass("chkb-checked").prop("checked", false);
    $(this).parent().find(".__ttip").hide();
  });
  <!-- /checkbox click -->
}
